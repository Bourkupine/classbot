import fs from 'fs';
import { Client, Collection, Intents, Interaction } from "discord.js";
import { env, version, token, name } from "./config";
import { PG_Manager } from "./db";

interface Custom_Command {
    data: {
        name: string,
    },
    execute: (interaction: Interaction, db: PG_Manager) => Promise<void>,
    setup?: (db: PG_Manager) => Promise<void>
}
// this extends teh normal Clients object with another field.
// more info on Collections here: https://discordjs.guide/additional-info/collections.html#array-like-methods
interface Client_Custom extends Client{
    commands: Collection<string, Custom_Command>
}


main().catch((err) => console.log(err));
async function main() {
    // this measures how long it takes to get up and running
    const start = new Date().getTime();

    // bit of trickery to make .commands work in ts
    const client: Client_Custom = Object.assign(
        new Client({
            intents: [Intents.FLAGS.GUILDS],
            presence: {
                activities: [
                    {
                        name: "your back",
                        type: "WATCHING",
                        url: "https://gitlab.com/c2842/misc/classbot",
                    },
                ],
            },
        }),
        { commands: new Collection<string, Custom_Command>() }
    );

    const connection = new PG_Manager();
    await connection.up();

    // dynamically import the commands
    // the path here is relative to teh root of the project
    const commandFiles = fs.readdirSync('./build/src/commands').filter(file => file.endsWith('.js'));
    for (const file of commandFiles) {
        // the path here is relative to this file
        const command: Custom_Command = require(`./commands/${file}`);
        // Set a new item in the Collection
        // With the key as the command name and the value as the exported module

        // if it needs some setup, creating tabls etc then thenis is the spot
        if (command.setup) {
            await command.setup(connection);
        }

        client.commands.set(command.data.name, command);
    }

    // how different events are handled
    client.once('ready', () => {console.log(`[${new Date().toISOString()}] [${env}] [v${version}] [${client.guilds.cache.size} guilds] [${(new Date().getTime() - start) / 1000}s startup for ${name}]`);});

    client.on('interactionCreate', async (interaction: Interaction) => {
        if (!interaction.isCommand()) return;

        const command = client.commands.get(interaction.commandName);


        if (!command) return;

        try {
            await command.execute(interaction, connection);
        }
        catch (error) {
            console.error(error);
            await interaction.reply({ content: 'There was an error while executing this command!', ephemeral: true });
        }
    });

    await client.login(token);
}