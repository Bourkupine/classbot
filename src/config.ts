import dotenv from 'dotenv';
import packageData from '../package.json';

dotenv.config();

export const name: string = packageData.name;
export const version: string = packageData.version;
export const env: string = process.env.enviroment || "production";
export const token: string = process.env.token;
export const clientId: string = process.env.clientId;