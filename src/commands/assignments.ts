/*
Creator:    Brendan Golden
Date:       2021-10-17
Description:This command is to manage the assignments channel
*/

import { SlashCommandBuilder, SlashCommandSubcommandBuilder } from '@discordjs/builders';
import { Interaction, MessageEmbedOptions, TextChannel } from "discord.js";
import { PG_Manager } from "../db";
import { Assignments, Channels } from "../db_entities";
import { FindConditions, MoreThan } from "typeorm";

const command_new = new SlashCommandSubcommandBuilder().setName('new').setDescription('Set a new assignment')
    .addStringOption(option => option.setName('module').setDescription('module the assignment is for').setRequired(true))
    .addStringOption(option => option.setName('title').setDescription('name of teh assignment').setRequired(true))
    .addStringOption(option => option.setName('due').setDescription('when is it due').setRequired(true))
    .addStringOption(option => option.setName('link').setDescription('link to the sulis page').setRequired(true));

const command_edit = new SlashCommandSubcommandBuilder().setName('edit').setDescription('Edit a new assignment')
    .addNumberOption(option => option.setName('id').setDescription('ID of the assignment').setRequired(true))
    .addStringOption(option => option.setName('module').setDescription('module the assignment is for'))
    .addStringOption(option => option.setName('title').setDescription('name of teh assignment'))
    .addStringOption(option => option.setName('due').setDescription('when is it due'))
    .addStringOption(option => option.setName('link').setDescription('link to the sulis page'));

const command_list = new SlashCommandSubcommandBuilder().setName('list').setDescription('List Assignments')
    .addBooleanOption(option => option.setName('all').setDescription('Lists all assignments, boolean'))
    .addStringOption(option => option.setName('module').setDescription('Filters assignments for a specific module'));

const command_delete = new SlashCommandSubcommandBuilder().setName('delete').setDescription('Edit a new assignment')
    .addNumberOption(option => option.setName('id').setDescription('ID of the assignment').setRequired(true));

const command = new SlashCommandBuilder().setName('assignment').setDescription('Replies with assignment!')
    .addSubcommand(command_new)
    .addSubcommand(command_list)
    .addSubcommand(command_edit)
    .addSubcommand(command_delete);


async function assignment_new(interaction: Interaction, db: PG_Manager) {
    if (!interaction.isCommand()) return;

    // needs to get the channel ID it posts out to
    const channel_id = await db.get_items(Channels, { server: interaction.guildId, command: "assignments" }, ["channel"]);

    if (channel_id.length == 0) {return await interaction.reply('Channel not set for command, use ``/admin set assignment`` in teh channel you wish to have it in');}

    // get the channel we are sending it to
    const channel = interaction.client.channels.cache.get(channel_id[0].channel) as TextChannel;

    // check the users permissions for teh channel
    const can_post = channel.permissionsFor(interaction.user).has("SEND_MESSAGES", true);
    if (!can_post) {return await interaction.reply(`You dont have permission to post in <#${channel_id[0].channel}>`);}

    // split out values
    const module = interaction.options.getString('module');
    const title = interaction.options.getString('title');
    const due = interaction.options.getString('due');
    const link = interaction.options.getString('link');

    // input validation
    if (!link.startsWith("https://")) {return await interaction.reply({ content: 'Link needs to start with https://', ephemeral: true });}

    const due_formatted = new Date(due);

    const result = await db.update_one(Assignments, {
        server: interaction.guildId,
        module: module,
        title:title,
        due: due_formatted.getTime(),
        link:link,
        user: interaction.user.id,
    });

    // create the embed
    const embed: MessageEmbedOptions = {
        title: `${module} - ${title}`,
        url: link,
        fields: [
            { name: 'Module', value: module, inline: true },
            { name: 'Name', value: title, inline: true },
            { name: 'Due', value: due_formatted.toISOString(), inline: true },
            { name: 'Link', value: `[${title}](${link})`, inline: false },
            { name: 'Added by', value: `<@${interaction.user.id}>`, inline: true },
            { name: 'ID', value: result.id.toString(), inline: true },
        ],
    };

    // send it off
    await channel.send({ embeds: [embed] });

    // send a reply to the command user
    return await interaction.reply({ content: 'Assignment set', ephemeral: true });
}

async function assignment_edit(interaction: Interaction, db: PG_Manager) {
    if (!interaction.isCommand()) return;

    // needs to get the channel ID it posts out to
    const channel_id = await db.get_items(Channels, { server: interaction.guildId, command: "assignments" }, ["channel"]);

    if (channel_id.length == 0) {return await interaction.reply('Channel not set for command, use ``/admin set assignment`` in teh channel you wish to have it in');}

    // get the channel we are sending it to
    const channel = interaction.client.channels.cache.get(channel_id[0].channel) as TextChannel;

    // check the users permissions for teh channel
    const can_post = channel.permissionsFor(interaction.user).has("SEND_MESSAGES", true);
    if (!can_post) {return await interaction.reply(`You dont have permission to post in <#${channel_id[0].channel}>`);}

    // always requires teh ID
    const modified: Partial<Assignments> = {
        id: interaction.options.getNumber('id'),
        user: interaction.user.id,
    };

    const module = interaction.options.getString('module');
    if (module) {
        modified.module = module;
    }

    const title = interaction.options.getString('title');
    if (title) {
        modified.title = title;
    }

    const due = interaction.options.getString('due');
    if (due) {
        modified.due = new Date(due).getTime();
    }

    const link = interaction.options.getString('link');
    if (!link.startsWith("https://")) {return await interaction.reply({ content: 'Link needs to start with https://', ephemeral: true });}
    if (link) {
        modified.link = link;
    }

    const updated = await db.update_one(Assignments, modified);

    // create the embed
    const embed: MessageEmbedOptions = {
        title: `${updated.module} - ${updated.title}`,
        url: updated.link,
        fields: [
            { name: 'Module', value: updated.module, inline: true },
            { name: 'Name', value: updated.title, inline: true },
            { name: 'Due', value: new Date(updated.due).toISOString(), inline: true },
            { name: 'Link', value: `[${updated.title}](${updated.link})`, inline: false },
            { name: 'Added by', value: `<@${updated.user}>`, inline: true },
            { name: 'ID', value: updated.id.toString(), inline: true },
        ],
    };

    // send it off
    await channel.send({ embeds: [embed] });

    // send a reply to the command user
    return await interaction.reply({ content: `Assignment #${updated.id} updated`, ephemeral: true });
}

async function assignment_delete(interaction: Interaction, db: PG_Manager) {
    if (!interaction.isCommand()) return;

    // needs to get the channel ID it posts out to
    const channel_id = await db.get_items(Channels, { server: interaction.guildId, command: "assignments" }, ["channel"]);

    if (channel_id.length == 0) {return await interaction.reply('Channel not set for command, use ``/admin set assignment`` in teh channel you wish to have it in');}

    // get the channel we are sending it to
    const channel = interaction.client.channels.cache.get(channel_id[0].channel) as TextChannel;

    // check the users permissions for teh channel
    const can_post = channel.permissionsFor(interaction.user).has("SEND_MESSAGES", true);
    if (!can_post) {return await interaction.reply(`You dont have permission to post in <#${channel_id[0].channel}>`);}

    // split out values
    const id = interaction.options.getNumber('id');

    await db.delete(Assignments, {
        id: id,
        server: interaction.guildId,
    });

    // send a reply to the command user
    return await interaction.reply({ content: `Assignment #${id} deleted`, ephemeral: true });
}

const toHMS = (secs) => {
    const sec_num = parseInt(secs, 10);
    const hours = Math.floor(sec_num / 3600);
    const minutes = Math.floor(sec_num / 60) % 60;
    const seconds = sec_num % 60;

    return [hours, minutes, seconds]
        .map(v => v < 10 ? "0" + v : v)
        .filter((v, i) => v !== "00" || i > 0)
        .join(":");
};

async function assignment_list(interaction: Interaction, db: PG_Manager) {
    if (!interaction.isCommand()) return;

    // see if #all has been pinged
    const all = interaction.options.getBoolean('all');
    const module_choice = interaction.options.getString('module');

    // construct teh query
    let query: FindConditions<Assignments>;
    if (module_choice) {
        if (all) {
            query = {
                server: interaction.guildId,
                module: module_choice,
            };
        } else {
            query = {
                server: interaction.guildId,
                module: module_choice,
                due: MoreThan(new Date().getTime()),
            };
        }
    } else if (all) {
        query = {
            server: interaction.guildId,
        };
    } else {
        query = {
            server: interaction.guildId,
            due: MoreThan(new Date().getTime()),
        };
    }

    // now search for it
    const result = await db.get_items(Assignments, query);
    if (result.length === 0) {
        return await interaction.reply({
            content: "No assignments match search",
            ephemeral: true,
        });
    }

    // newest first
    result.sort((a, b) => {
        return new Date(a.due).getTime() - new Date(b.due).getTime();
    }).reverse();

    const embeds = [];

    // only get thsi value once
    const now_epoch = new Date().getTime();
    // only 10 embeds are allowed
    for (let i = 0;i < result.length;i++) {
        const { module, title, link, due, user, id } = result[i];

        let remaining = "00:00:00";
        if (due > now_epoch) {
            remaining = toHMS((due - now_epoch) / 1000);
        }

        const embed = {
            title: `${module} - ${title}`,
            url: link,
            fields: [
                { name: 'Module', value: module, inline: true },
                { name: 'Name', value: title, inline: true },
                { name: 'Due', value: new Date(due).toISOString(), inline: true },
                { name: 'Time Remaining (hh:mm:ss)', value: remaining, inline: false },
                { name: 'Link', value: `[${title}](${link})`, inline: false },
                { name: 'Added by', value: `<@${user}>`, inline: true },
                { name: 'ID', value: id.toString(), inline: true },
            ],
        };
        embeds.push(embed);

        if (i == 9) {
            break;
        }
    }

    // send a reply to the command user
    return await interaction.reply({ embeds:embeds, ephemeral: true });
}

module.exports = {
    data: command,

    // its named execute to standardise it across modules
    async execute(interaction: Interaction, db: PG_Manager) {
        // need to have this here for types to work properly
        if (!interaction.isCommand()) return;

        // split out by the subcommand
        switch (interaction.options.getSubcommand()) {
            case "new" : {
                return await assignment_new(interaction, db);
            }
            case "list" : {
                return await assignment_list(interaction, db);
            }
            case "edit" : {
                return await assignment_edit(interaction, db);
            }
            case "delete" : {
                return await assignment_delete(interaction, db);
            }
            default: {
                await interaction.reply('Please select a subcommand');
            }
        }
    },
};