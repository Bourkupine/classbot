/*
Creator:    Brendan Golden
Date:       2021-10-26
Description:This command is (currently) to set what channels the bot talks in)
*/


import { SlashCommandBuilder, SlashCommandSubcommandBuilder } from '@discordjs/builders';
import { Interaction, TextChannel } from "discord.js";
import { PG_Manager } from "../db";
import { Channels } from "../db_entities";

const set = new SlashCommandSubcommandBuilder().setName('set').setDescription('Set a channel to output a command')
    .addStringOption(option => option.setName('command').setDescription('Command to set the channel for').setRequired(true));

const deletion = new SlashCommandSubcommandBuilder().setName('delete').setDescription('Set a channel to output a command')
    .addStringOption(option => option.setName('command').setDescription('Command to delete the channel for').setRequired(true));

const main = new SlashCommandBuilder().setName('admin').setDescription('Admin commands')
    .addSubcommand(set)
    .addSubcommand(deletion);

async function channel_set(interaction: Interaction, db: PG_Manager) {
    // sets the channel for teh bot if the user has admin perms
    if (!interaction.isCommand()) return;

    // get the channel we are sending it to
    const channel = interaction.client.channels.cache.get(interaction.channelId) as TextChannel;

    // Only admin can set channels
    const admin = channel.permissionsFor(interaction.user).has("ADMINISTRATOR", true);
    if (!admin) {return await interaction.reply('Only admin can set the channel');}

    const command = interaction.options.getString('command');

    await db.update_one(Channels, {
        server: interaction.guildId,
        channel: interaction.channelId,
        command: command,
        user: interaction.user.id,
    });

    return await interaction.reply(`<#${interaction.channelId}> selected for ${command}`);
}

async function channel_delete(interaction: Interaction, db: PG_Manager) {
    // sets the channel for teh bot if the user has admin perms
    if (!interaction.isCommand()) return;

    // get the channel we are sending it to
    const channel = interaction.client.channels.cache.get(interaction.channelId) as TextChannel;

    // Only admin can set channels
    const admin = channel.permissionsFor(interaction.user).has("ADMINISTRATOR", true);
    if (!admin) {return await interaction.reply('Only admin can set the channel');}

    const command = interaction.options.getString('command');

    await db.delete(Channels, {
        server: interaction.guildId,
        channel: interaction.channelId,
        command: command,
    });

    return await interaction.reply(`<#${interaction.channelId}> deselected for ${command}`);
}

module.exports = {
    data: main,

    // its named execute to standardise it across modules
    async execute(interaction: Interaction, db: PG_Manager) {
        // need to have this here for types to work properly
        if (!interaction.isCommand()) return;

        // split out by the subcommand
        switch (interaction.options.getSubcommand()) {
            case "set" : {
                return await channel_set(interaction, db);
            }
            case "delete" : {
                return await channel_delete(interaction, db);
            }
            default: {
                await interaction.reply('Please select a subcommand');
            }
        }
    },
};