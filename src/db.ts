/*
This layout and the way teh entities are laid out si taken from anther project of mine,
essentially provides a nice wrapper around typeorm
*/


import "reflect-metadata";
import { Connection, createConnection, DeepPartial, EntitySchema, FindConditions } from "typeorm";

import { entities } from "./db_entities";

export class PG_Manager {
    private connection: Connection;

    public async up() {
        console.time("DB: Connecting");
        this.connection = await createConnection({
            name: new Date().toISOString(),
            type: "sqlite",
            database: `./database.db`,
            entities: entities,
            synchronize: true,
            logging: false,
        }).catch(error => {
            console.log(error);
            process.exit(1);
        });
        console.timeEnd("DB: Connecting");
    }

    public async down() {
        await this.connection.close();
    }

    public async update_one<T>(repo: string | (new () => T) | EntitySchema<T>, item: DeepPartial<T>): Promise<DeepPartial<T> & T> {
        return await this.connection.getRepository<T>(repo).save(item);
    }

    public async delete<T>(repo: string | (new () => T) | EntitySchema<T>, item: FindConditions<T>) {
        await this.connection.getRepository<T>(repo).delete(item);
    }

    public async update_bulk<T>(repo: string | (new () => T) | EntitySchema<T>, items: DeepPartial<T>[], limit = 100000) {
        const repository = this.connection.getRepository<T>(repo);

        let saving: DeepPartial<T>[] = [];
        for (let i = 0;i < items.length;i++) {
            saving.push(items[i]);

            if (saving.length === limit) {
                console.log(i, items.length);
                // save those items
                await repository.save(saving)
                    .catch(err => console.log(err));

                // reset the bulker
                saving = [];
            }
        }
        // catch any that werent caught on teh alst loop
        await repository.save(saving)
            .catch(err => console.log(err));
    }

    public async get_items<T>(repo: string | (new () => T) | EntitySchema<T>, query: FindConditions<T>, project?: string[]):Promise<T[]> {
        const repository = this.connection.getRepository<T>(repo);

        const find_options = { where: query };
        if (project) {
            find_options["select"] = project;
        }

        return await repository.find(find_options).catch(err => {
            console.log(err);
            return [];
        });
    }
}
